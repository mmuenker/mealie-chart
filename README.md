# Mealie Helm Chart

This Helm chart deploys the mealie server on a Kubernetes cluster.

## Installation

To install the application using this Helm chart, follow these steps:

### Step 1: Prepare Configuration

Create a `custom.yaml` file to override the default configuration settings. Below is an example configuration that integrates with Traefik for ingress.

```yaml
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: traefik
  hosts:
    - host: sub.my-domain.com
      paths:
        - path: /
          pathType: ImplementationSpecific

postgres:
  enabled: false
  user: mealie
  password: mealie
  server: postgres
  port: 5432
  db: mealie

ldap:
  enabled: false
  serverUrl: 192.168.0.30
  baseDn: dc=ldap,dc=domain,dc=de
  queryBind: uid=root,cn=users,dc=ldap,dc=domain,dc=de
  userFilter: (memberOf=cn=mealie_user,dc=example,dc=com)
  adminFilter: (memberOf=cn=admins,dc=example,dc=com)
  idAttribute: id
  nameAttribute: name
  mailAttribute: email

```

If required the version of the docker image can be overridden by setting the `image.tag` value in the `custom.yaml` file.

```yaml
image:
  repository: ghcr.io/mealie-recipes/mealie
  tag: 9.8.7
  pullPolicy: IfNotPresent
```

#### Add the helm repository

The Helm chart is hosted in the GitLab Package Registry. To add the repository to Helm, use the following command:

```bash
helm repo add mealie https://gitlab.com/api/v4/projects/57589339/packages/helm/release
```

It is possible to use pre-release versions of the chart by replacing `release` with the pre-release channel name. The following pre-release channels are available:

- `nightly`
- `preview`
- `rc`

> **Note:** The pre-release channels may contain unstable or untested versions of the chart.

### Step 2: Deploy

Run the following Helm command to deploy with the custom configuration.

> **Note:** If a pre-release channel is used, add `--devel` to the Helm command.

```bash
helm install \
  --create-namespace \
  --namespace mealie \
  mealie \
  -f custom.yaml \
  mealie/mealie
```

## Upgrades

To upgrade your deployment to a new version or to apply configuration changes, use the following command:

```bash
helm upgrade \
  --namespace mealie \
  mealie \
  -f custom.yaml \
  mealie/mealie
```

## Additional Information

For more detailed configuration options and advanced setups, refer to the chart's `values.yaml` file and the Audiobookshelf documentation.


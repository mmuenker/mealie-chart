# Changelog

## [0.0.1-nightly.22](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.21...v0.0.1-nightly.22) (2025-03-01)


### Bug Fixes

* update app version to v2.7.1 ([6dd5762](https://gitlab.com/mmuenker/mealie-chart/commit/6dd5762a8adee114b3810f69c7cfa818bc88f431))

## [0.0.1-nightly.21](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.20...v0.0.1-nightly.21) (2025-02-28)


### Bug Fixes

* update app version to v2.7.0 ([82d809d](https://gitlab.com/mmuenker/mealie-chart/commit/82d809d0022717a1a7d047c2ae50ae68d464f0f8))

## [0.0.1-nightly.20](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.19...v0.0.1-nightly.20) (2025-02-06)


### Bug Fixes

* update app version to v2.6.0 ([7f5a721](https://gitlab.com/mmuenker/mealie-chart/commit/7f5a7212d779fb2674bacfb92f89f2b5b8ab235a))

## [0.0.1-nightly.19](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.18...v0.0.1-nightly.19) (2025-01-23)


### Bug Fixes

* update app version to v2.5.0 ([0561b8e](https://gitlab.com/mmuenker/mealie-chart/commit/0561b8e336e4cd65a51f8fa7b6b79ffc02318adb))

## [0.0.1-nightly.18](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.17...v0.0.1-nightly.18) (2025-01-08)


### Bug Fixes

* update app version to v2.4.2 ([963423b](https://gitlab.com/mmuenker/mealie-chart/commit/963423b78d3178a850f6518883696fa2c08a68ea))

## [0.0.1-nightly.17](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.16...v0.0.1-nightly.17) (2024-12-19)


### Bug Fixes

* update app version to v2.4.1 ([2b75aeb](https://gitlab.com/mmuenker/mealie-chart/commit/2b75aebcdbc8b075f150879ac7e58719b8b75b66))

## [0.0.1-nightly.16](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.15...v0.0.1-nightly.16) (2024-12-14)


### Bug Fixes

* update app version to v2.4.0 ([1656511](https://gitlab.com/mmuenker/mealie-chart/commit/1656511ce2a060cbb043d4475066f6b35bd1b8db))

## [0.0.1-nightly.15](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.14...v0.0.1-nightly.15) (2024-11-28)


### Bug Fixes

* update app version to v2.3.0 ([5964f6c](https://gitlab.com/mmuenker/mealie-chart/commit/5964f6ccb18a5ef1e2045b38156640a00714cf94))

## [0.0.1-nightly.14](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.13...v0.0.1-nightly.14) (2024-11-12)


### Bug Fixes

* update app version to v2.2.0 ([5f104f6](https://gitlab.com/mmuenker/mealie-chart/commit/5f104f68e43c2a6d589e9de815d8785980b5dcc9))

## [0.0.1-nightly.13](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.12...v0.0.1-nightly.13) (2024-10-31)


### Bug Fixes

* update app version to v2.1.0 ([0119119](https://gitlab.com/mmuenker/mealie-chart/commit/01191199f6e457f33510931686cbd5066b5e99e7))

## [0.0.1-nightly.12](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.11...v0.0.1-nightly.12) (2024-10-23)


### Bug Fixes

* update app version to v2.0.0 ([c755106](https://gitlab.com/mmuenker/mealie-chart/commit/c7551062ff210980f571f6d19a9860888ebb8539))

## [0.0.1-nightly.11](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.10...v0.0.1-nightly.11) (2024-08-22)


### Bug Fixes

* update app version to v1.12.0 ([b689cb5](https://gitlab.com/mmuenker/mealie-chart/commit/b689cb58326b872075eb997c63bdf6dab303bf07))

## [0.0.1-nightly.10](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.9...v0.0.1-nightly.10) (2024-08-01)


### Bug Fixes

* update app version to v1.11.0 ([65087ca](https://gitlab.com/mmuenker/mealie-chart/commit/65087ca2d7766c5866e52bcd09461d6b29f35359))

## [0.0.1-nightly.9](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.8...v0.0.1-nightly.9) (2024-07-06)


### Bug Fixes

* update app version to v1.10.2 ([f7668b6](https://gitlab.com/mmuenker/mealie-chart/commit/f7668b64a343501999eaadd6975602dfaaa0a020))

## [0.0.1-nightly.8](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.7...v0.0.1-nightly.8) (2024-07-04)


### Bug Fixes

* update app version to v1.10.1 ([a2ca013](https://gitlab.com/mmuenker/mealie-chart/commit/a2ca013f3cc16879512a41487646a5bf86870b9d))

## [0.0.1-nightly.7](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.6...v0.0.1-nightly.7) (2024-06-19)


### Bug Fixes

* update app version to v1.9.0 ([8413b44](https://gitlab.com/mmuenker/mealie-chart/commit/8413b44f62b431cc5909c5a23c7c543c0ae5fcff))

## [0.0.1-nightly.6](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.5...v0.0.1-nightly.6) (2024-06-06)


### Bug Fixes

* update app version to v1.8.0 ([ab1d024](https://gitlab.com/mmuenker/mealie-chart/commit/ab1d0245d9e6f814adb6d91b2836dc18060c7c38))

## [0.0.1-nightly.5](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.4...v0.0.1-nightly.5) (2024-05-24)


### Bug Fixes

* update app version to v1.7.0 ([97bad30](https://gitlab.com/mmuenker/mealie-chart/commit/97bad301c25a5d08f84f9cf895859b0414182790))

## [0.0.1-nightly.4](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.3...v0.0.1-nightly.4) (2024-05-08)


### Bug Fixes

* ensure all number and boolean env values are quoted ([0167094](https://gitlab.com/mmuenker/mealie-chart/commit/0167094c108dcdcc571a6d1175560bea930e2987))

## [0.0.1-nightly.3](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.2...v0.0.1-nightly.3) (2024-05-08)


### Bug Fixes

* add missing optional ldap variables ([697b5fa](https://gitlab.com/mmuenker/mealie-chart/commit/697b5fa65530ede58e018fb9baf9b19007277451))

## [0.0.1-nightly.2](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.1...v0.0.1-nightly.2) (2024-05-08)


### Bug Fixes

* update app version to v1.6.0 ([15617f1](https://gitlab.com/mmuenker/mealie-chart/commit/15617f18a1e6dcaec31bfcda007434cdad857eaa))

## [0.0.1-nightly.1](https://gitlab.com/mmuenker/mealie-chart/compare/v0.0.1-nightly.0...v0.0.1-nightly.1) (2024-05-07)


### Bug Fixes

* add missing optional ldap variable queryPassword ([7b9b1b6](https://gitlab.com/mmuenker/mealie-chart/commit/7b9b1b66f035fdf835de59102c5fce07ef746810))

## 0.0.1-nightly.0 (2024-05-06)
